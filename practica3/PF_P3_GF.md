# Practica 3

## Integrantes del grupo

- Josué Carlos Pérez Montenegro - 201403546
- Luis Gerardo Chay Grijalva    - 201700345
- Kelvin Manfredy Vasquez Gomez - 201212490

## Links de los contenedores

## Estructura del Dockerfile del Backend

``` Dockerfile
# Imagen de node en docker hub y especificando la version
FROM node:14.17 

# Apartado de LABEL para organizar la version y descripción de la imagen
# Cuando se ejecuta la imagen, estos label aparecen en la construcción dando una mejora visual
# Ejemplo: docker build -t label-demo
LABEL version="1.0"
LABEL description="This is the docker image for SA BackEnd"
LABEL maintainer = ["gerardo15.chay97@gmail.com"]

# Define el directorio de trabajo
WORKDIR /app 
# Copia todos los archivos al directorio creado
COPY package*.json ./
# Ejecuta el comando especificado, que es instalar las dependencias
RUN npm install 
# Copia todo al direcotrio de trabajo
COPY . .

# Expone la imagen en el puerto 3000
EXPOSE 3000
# Ejecuta el comando para levantar el servidro
CMD [ "npm", "start" ]
```

## Estructura del DockerCompose

``` yml
# Se escoge la versión
version: "3"

# El bloque de SERVICES, es donde se declara las imagenes que se crearan
services:
  sa-p3-server: # Inicio de servicio de backend
    image: sa-p3-server # Se le coloca nombre a la imagen
    build: # apartado para crear o referenciar la imagen
      dockerfile: Dockerfile # nombre del archivo o de la imagen
      context: ./Backend # path de donde se encuentra el Dockerfile
    links: # apartado de imagenes que tiene que esperar que esten lista
      - mongo
    environment: # aparatdo de variables de entorno
      PORT: 3000
    ports: # se define o se setea el puerto
      - "3000:3000" 
    networks: # se especifica a que redes esta ligado
      - backend-network
      - mongodb-network
      
  mongo: #imagen de mongo
    container_name: mongo # hace referencia la imagen de mongo de DockerHub
    restart: always # Se restaurara siempre que se caiga
    image: mongo # nombre de imagen
    ports: # apartado de puertos
      - "27017:27017"
    volumes: # se define la carpeta donde se manejaran los volumes, para que los datos prevalezcas
      - ~/mongo/data:/data/db
    networks: # se especifica a que redes esta ligado
      - mongodb-network

# Bloque donde se definen las diferentes redes
networks:
  frontend-network:
    driver: bridge
  mongodb-network:
    driver: bridge
  backend-network:
    driver: bridge
```

## Mostrar la carpeta local que es utilizada como volumen

El volumen para la base de datos fue creadad en la siguiente carpeta

``` yml
  volumes:
      - ~/mongo/data:/data/db
```

## Captura que compruebe la creación de artefactos

## Captura de ejecución de las pruebas unitarias

## Captura de los resultados de cobertura con sonarqube
