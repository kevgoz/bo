const mongoose = require('mongoose');

mongoose.connect(
  'mongodb://mongo:27017/practica3',
  { useNewUrlParser: true}
  )
  .then(db => console.log('DB is conected to ', db.connection.host))
  .catch(err => console.error(err));