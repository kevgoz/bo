const express = require('express'),
  cors = require('cors'),
  morgan = require('morgan'),
  config = require('../config');

// SE CONECTA A MONGO
require('./database');

const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(cors({ origin: true }))
app.use(morgan('dev'))

app.use(require('./routes/index'))

app.listen(config.PORT, () => console.log(`Server on port ${config.PORT}`))