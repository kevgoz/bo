const { Router } = require('express'),
  Estudiante = require('../models/estudiante');

const router = Router()

// DEVUELVE TODOS LOS ESTUDIANTES AGREGADOS
router.get('/all', (req, res) => {
  Estudiante.find()
    .exec()
    .then(x => res.status(200).json({ status: true, value: x, message: '' }))
    .catch(err => {
      console.log('Error en GET all ', err);
      res.status(200).json({ status: false, value: [], message: 'No se puedo obtener a los estudiantes' }) 
    });
});

// DEVUELVE UN ESTUDIANTE POR SU CARNET
router.get('/one/:carnet', (req, res) => {
  const { carnet } = req.params;

  Estudiante.findOne({carnet: carnet})
    .exec()
    .then(x => res.status(200).json({ status: true, value: x, message: '' }))
    .catch(err => {
      console.log(`Error en GET one ${carnet}`, err);
      res.status(200).json({ status: false, value: null, message: `No se puedo obtener al estudiante ${carnet}` }) 
    });
});

// AGREGA UN NUEVO ESTUDIANTE
router.post('/add', (req, res) => {
  const { carnet, nombre } = req.body;
  
  if(!carnet) {
    console.log(`Error en POST add, carnet es vacio o undefined`, err);
    return res.status(200).json({ status: false, value: null, message: `CARNET es vacio o undefined` }) 
  }

  if(!nombre) {
    console.log(`Error en POST add, nombre es vacio o undefined`, err);
    return res.status(200).json({ status: false, value: null, message: `NOMBRE es vacio o undefined` }) 
  }

  const newEstudiantee = {
    carnet: carnet,
    nombre: nombre
  }

  Estudiante.create(newEstudiantee)
    .then(x => res.status(201).json({ status: true, value: x, message: '' }))
    .catch(err => {
      console.log(`Error en POST add, no se agrego al estudiante: ${carnet}`, err);
      res.status(200).json({ status: false, value: null, message: `No se puedo agregar al estudiante ${carnet}` }) 
    });
});

// ACTUALIZA UN ESTUDIANTE
router.put('/update/:id', (req, res) => {
  const { id } = req.params;
  const { carnet, nombre } = req.body;
  
  if(!carnet) {
    console.log(`Error en POST add, carnet es vacio o undefined`, err);
    return res.status(200).json({ status: false, value: null, message: `CARNET es vacio o undefined` }) 
  }

  if(!nombre) {
    console.log(`Error en POST add, nombre es vacio o undefined`, err);
    return res.status(200).json({ status: false, value: null, message: `NOMBRE es vacio o undefined` }) 
  }

  const setEstudiante = {
    carnet: carnet,
    nombre: nombre
  }

  Estudiante.findOneAndUpdate({_id: id}, setEstudiante, { new: true})
    .then(x => res.status(201).json({ status: true, value: x, message: '' }))
    .catch(err => {
      console.log(`Error en PUT update, no se actualizo al estudiante: ${carnet}`, err);
      res.status(201).json({ status: false, value: null, message: `No se puedo actualizar al estudiante ${carnet}` }) 
    });
});

// ELIMINA UN ESTUDIANTE
router.delete('/:id', (req, res) => {
  const { id } = req.params;

  Estudiante.findOneAndDelete({_id: id})
    .exec()
    .then(x => res.status(200).json({ status: true, value: x, message: '' }))
    .catch(err => {
      console.log(`Error en DELETE one ${id}`, err);
      res.status(200).json({ status: false, value: null, message: `No se puedo eliminar al estudiante` }) 
    });
});
module.exports = router