const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EstudianteSchema = new Schema({
  carnet: {
    type: String,
    required: true
  },
  nombre: {
    type: String,
    required: true
  }
});

module.exports = Estudiante = mongoose.model('estudiante', EstudianteSchema);